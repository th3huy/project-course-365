/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
var gCoursesDB = [];

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    callApiGetCourse();
};

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm xử lý khóa học từ api
function callApiGetCourse() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        dataType: "json",
        success: function (dataObj) {
            gCoursesDB = dataObj;
            // hàm lấy khóa học Recommended( lấy 4 phần tử đầu tiên)
            loadCourseToElement(gCoursesDB.slice(0, 4), ".recommend-course");
            // hàm lấy khóa học Most Popular (lấy 4 phần tử có isPopular true đầu tiên)
            loadCourseToElement(filterCoursesByProperty(gCoursesDB, "isPopular", true).slice(0, 4), ".popular-course");
            // hàm lấy khóa học Trending (lấy 4 phần tử có isTrending true đầu tiên)
            loadCourseToElement(filterCoursesByProperty(gCoursesDB, "isTrending", true).slice(0, 4), ".trending-course");
        },
        error: function (ajaxContext) {
            alert(ajaxContext);
        }
    });
};

// hàm load ra khóa học
function loadCourseToElement(courses, elementSelector) {
    courses.forEach(course => {
        $(elementSelector).append(`
            <div class="card-course col-3">
                <img src="${course.coverImage}" alt="">
                <div class="card-body">
                    <p>${course.courseName}</p>
                    <div class="group-1">
                        <i class="far fa-clock fa-xs"></i>
                        <span>${course.duration}</span>
                        <span style="margin-left: 6px;">${course.level}</span>
                    </div>
                    <p>
                        <span class="span-first">${course.price}</span> 
                        <span class="span-last">${course.discountPrice}</span>
                    </p>
                </div>
                <div class="card-foot">
                    <img src="${course.teacherPhoto}" alt="">
                    <span>${course.teacherName}</span>
                    <i class="far fa-bookmark fa-xs" style="margin-left: 62px;"></i>
                </div>
            </div>
        `);
    });
};

//hàm lọc khóa học
function filterCoursesByProperty(courses, propertyName, value) {
    return courses.filter(course => course[propertyName] === value);
}