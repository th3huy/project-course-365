## ✨ Method
-   **DataTable ,clear(),add(),draw()**: là các phương thức dùng trong thư viện DataTable để xóa,thêm và vẽ lại bảng
-   **$().on**, 
-   **$().parents** :là một phương thức được sử dụng để trả về tất cả các phần tử cha của các phần tử được chọn.,
-   **$().attr**: được sử dụng để lấy giá trị của thuộc tính của phần tử hoặc đặt giá trị cho một thuộc tính cụ thể của phần tử. ,
-   **$().val**: được sử dụng để lấy giá trị của phần tử đầu tiên trong tập hợp được chọn, hoặc đặt giá trị cho phần tử đầu tiên trong tập hợp được chọn.,
-   **appendTo()**:được sử dụng để chèn (append) nội dung của các phần tử được chọn vào cuối mỗi phần tử trong tập hợp được chỉ định.,
## 🌟 What is it
[Mid Module Exam][Jquery & Bootstrap] - **Project Product Management**
-   **Danh sách các khóa học**:Mã khóa học, Tên khóa học, Giá niêm yết,Giá bán thực tế,... (software).
-   **Modal từng sản phẩm**  (software).
## 💻 Function
 🔥 **Danh sách sản phẩm** ![Alt](/public/images/Untitled.png)
 🔥 **Chi tiết sản phẩm** ![Alt](/public/images/details.png)

## 🧱 Technology Stack and Requirements

-   [JavaScript](https://www.w3schools.com/js/) language
-   [jQuery](https://jquery.com/) 
-   [Awesome font](https://fontawesome.com/)
-   [AJax](https://api.jquery.com/jQuery.ajax/)
-   [Modal](https://getbootstrap.com/docs/4.0/components/modal/)
-   [DataTable](https://datatables.net/)